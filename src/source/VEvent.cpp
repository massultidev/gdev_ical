/* Copyright by: P.J. Grochowski */

#include "gdev/iCal/VEvent.h"

#include <Poco/Exception.h>

namespace gdev {
namespace iCal {

const std::string VEvent::VEVENT_BEGIN 		= "BEGIN:VEVENT";
const std::string VEvent::VEVENT_END 		= "END:VEVENT";
const std::string VEvent::KEY_DTSTART 		= "DTSTART";
const std::string VEvent::KEY_DTEND 		= "DTEND";
const std::string VEvent::KEY_SUMMARY 		= "SUMMARY";
const std::string VEvent::KEY_DESCRIPTION 	= "DESCRIPTION";
const std::string VEvent::KEY_LOCATION 		= "LOCATION";
const std::string VEvent::PARAM_TZID 		= "TZID";

VEvent::VEvent( std::vector < std::string > lines ) {
	if( !lines.empty() ) {
		this->internalParse( lines );
	}
}
VEvent::~VEvent() {
}

VEvent::Ptr VEvent::parse( std::vector < std::string > lines ) {
	return new VEvent( lines );
}

void VEvent::internalParse( std::vector < std::string > lines ) {
    for( std::size_t i=0; i<lines.size(); i++ )
    {
    	if(
    			( lines.at( i ).find( VEVENT_BEGIN ) != std::string::npos )
			||
				( lines.at( i ).find( VEVENT_END ) != std::string::npos )
		)
    	{
    		continue;
    	}

        std::size_t pos = lines.at( i ).find( ":" );
        if( pos == std::string::npos ) {
        	throw Poco::Exception( "Missing colon in line: '%s'", lines.at( i ) );
        }
        if( pos < 1 ) {
        	throw Poco::Exception( "Missing content before colon in line: '%s'", lines.at( i ) );
        }

        // Get key:
        std::string key = lines.at( i ).substr( 0, pos );

        // Get value:
        std::string value = lines.at( i ).substr( pos+1 );

        // Check if has parameter:
        std::string param = "";
        pos = key.find( ";" );
        if( pos != std::string::npos )
        {
            // Separate key from parameter:
            param = key.substr( pos+1 );
            key = key.substr( 0, pos );
        }

        // Add data to map:
        mKeys[ key ] = std::pair < std::string, std::string > ( param, value );
    }
}

bool VEvent::hasKey( const std::string& key ) const {
	return mKeys.count( key );
}
bool VEvent::hasParam( const std::string& key ) const {
    if( !mKeys.count( key ) ) {
        throw Poco::Exception( "Event does not have requested key!" );
    }
    return mKeys[ key ].first.size();
}

std::string VEvent::getValue( const std::string& key ) const {
    if( !mKeys.count( key ) ) {
        throw Poco::Exception( "Event does not have requested key!" );
    }
    return mKeys[ key ].second;
}
std::string VEvent::getParam( const std::string& key ) const {
    if( !mKeys.count( key ) ) {
        throw Poco::Exception( "Event does not have requested key!" );
    }
    return mKeys[ key ].first;
}

std::vector < std::string > VEvent::getKeys() const {
    std::vector < std::string > keys;
    for( std::map < std::string, std::pair < std::string, std::string > >::iterator
    		it = mKeys.begin(), itEnd = mKeys.end(); it != itEnd; it++ )
    {
        keys.push_back( it->second.first );
    }
    return keys;
}

std::string VEvent::toString() const {
    std::string strEvent = "";
    for( std::map < std::string, std::pair < std::string, std::string > >::const_iterator
    		it = mKeys.begin(), itEnd = mKeys.end(); it != itEnd; it++ )
    {
        if( it->second.first.size() )
        {
            strEvent += it->first + ";" + it->second.first + ":" + it->second.second + "\n";
        } else {
            strEvent += it->first + ":" + it->second.second + "\n";
        }
    }
    return strEvent;
}

}
}
