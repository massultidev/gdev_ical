/* Copyright by: P.J. Grochowski */

#include "gdev/iCal/ICalParser.h"

#include <Poco/Exception.h>
#include <Poco/Format.h>
#include <Poco/StringTokenizer.h>

namespace gdev {
namespace iCal {

ICalParser::ICalParser( const std::string& iCalFileContent ):
		mPosBegin( std::string::npos ),
		mPosEnd( std::string::npos )
{
	// Split lines:
	std::vector < std::string > lines;
	{
		Poco::StringTokenizer st( iCalFileContent, "\r\n", Poco::StringTokenizer::TOK_IGNORE_EMPTY|Poco::StringTokenizer::TOK_TRIM );
		lines = std::vector < std::string >( st.begin(), st.end() );
	}
	if( lines.empty() ) {
		throw Poco::Exception( "There are no lines!" );
	}

	// Verify structure:
	const std::string VCALENDAR_BEGIN 	= "BEGIN:VCALENDAR";
	const std::string VCALENDAR_END 	= "END:VCALENDAR";
    for( std::size_t i=0; i<lines.size(); i++ )
    {
        if(
        		( mPosBegin != std::string::npos )
			&&
				( mPosEnd != std::string::npos )
		)
        {
        	break;
        }

        if( mPosBegin == std::string::npos )
        {
            if( lines.at( i ).find( VCALENDAR_BEGIN ) != std::string::npos ) {
            	mPosBegin = i+1;
            }
        }

        if( mPosEnd == std::string::npos )
        {
            if( lines.at( i ).find( VCALENDAR_END ) != std::string::npos ) {
            	mPosEnd = i;
            }
        }
    }
    if( mPosBegin == std::string::npos ) {
    	throw Poco::Exception( Poco::format( "Missing '%s' statement!", VCALENDAR_BEGIN ) );
    }
    if( mPosEnd == std::string::npos ) {
    	throw Poco::Exception( Poco::format( "Missing '%s' statement!", VCALENDAR_END ) );
    }

    // Fix commas:
    for( std::size_t i=0; i<lines.size(); i++ )
    {
        std::size_t pos = lines.at( i ).find( "\\," );
        while( pos != std::string::npos )
        {
            lines.at( i ).erase( pos, 1 );
            pos = lines.at( i ).find( "\\," );
        }
    }

    parse( lines );
}
ICalParser::~ICalParser() {
}

void ICalParser::parse( const std::vector < std::string >& lines ) {
	// Find events:
	std::vector < std::size_t > vectPosBegin;
	std::vector < std::size_t > vectPosEnd;
	for( std::size_t i=mPosBegin+1; i<mPosEnd; i++ )
	{
		if( lines.at( i ).find( VEvent::VEVENT_BEGIN ) != std::string::npos ) {
			vectPosBegin.push_back( i );
		}
		if( lines.at( i ).find( VEvent::VEVENT_END ) != std::string::npos ) {
			vectPosEnd.push_back( i );
		}
	}

	// Check events amount correctness:
	if( vectPosBegin.size() != vectPosEnd.size() ) {
		throw Poco::Exception( "Amount of beginning event indicators is not equal to ones ending event!" );
	}

	// Check if not empty:
	if( vectPosBegin.empty() ) {
		throw Poco::Exception( "There are no events!" );
	}

	// Check events bounds correctness:
	for( std::size_t i=0; i<vectPosBegin.size(); i++ )
	{
		if( vectPosBegin.at( i ) > vectPosEnd.at( i ) ) {
			throw Poco::Exception( Poco::format( "Event by number '%?d' has swapped beginning and ending statements!", i ) );
		}
	}

	// Create events:
	{
		const std::string ERR_FMT = "Exception during parsing event '%?d': %s";
		for( std::size_t i=0; i<vectPosBegin.size(); i++ )
		{
			// Get one event lines:
			std::size_t idx1 = vectPosBegin.at( i );
			std::size_t idx2 = vectPosEnd.at( i );
			std::vector < std::string > eventLines( lines.begin() + idx1, lines.begin() + idx2 );

			// Create event:
			try {
				mEvents.push_back( new VEvent( eventLines ) );
			}
			catch( const Poco::Exception& e ) {
				throw Poco::Exception( Poco::format( ERR_FMT, i, e.displayText() ) );
			}
			catch( const std::exception& e ) {
				throw Poco::Exception( Poco::format( ERR_FMT, i, std::string( e.what() ) ) );
			}
			catch( ... ) {
				throw Poco::Exception( Poco::format( ERR_FMT, i, std::string( "Unknown exception!" ) ) );
			}
		}
	}
}

std::vector<VEvent::Ptr> ICalParser::getEvents() const {
	return mEvents;
}

}
}
