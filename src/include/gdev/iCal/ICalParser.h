/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_ICAL_ICALPARSER_H_
#define SRC_INCLUDE_GDEV_ICAL_ICALPARSER_H_

#include "VEvent.h"

#include <string>
#include <vector>

namespace gdev {
namespace iCal {

class ICalParser
{
	public:
		ICalParser(const std::string& iCalFileContent="");
		virtual ~ICalParser();

		std::vector<VEvent::Ptr> getEvents() const;

	private:
		void parse(const std::vector<std::string>& lines );

		std::size_t mPosBegin;
		std::size_t mPosEnd;
		std::vector<VEvent::Ptr> mEvents;
};

}
}

#endif
