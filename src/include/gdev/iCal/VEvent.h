/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_ICAL_VEVENT_H_
#define SRC_INCLUDE_GDEV_ICAL_VEVENT_H_

#include <Poco/SharedPtr.h>

#include <string>
#include <vector>
#include <map>

namespace gdev {
namespace iCal {

class VEvent
{
	public:
		typedef Poco::SharedPtr<VEvent> Ptr;

		static const std::string VEVENT_BEGIN; // 		= "BEGIN:VEVENT";
		static const std::string VEVENT_END; // 		= "END:VEVENT";

		static const std::string KEY_DTSTART; // 		= "DTSTART";
		static const std::string KEY_DTEND; // 			= "DTEND";
		static const std::string KEY_SUMMARY; // 		= "SUMMARY";
		static const std::string KEY_DESCRIPTION; // 	= "DESCRIPTION";
		static const std::string KEY_LOCATION; // 		= "LOCATION";

		static const std::string PARAM_TZID; // 		= "TZID";

		VEvent(std::vector<std::string> lines = std::vector<std::string>());
		virtual ~VEvent();

		static VEvent::Ptr parse(std::vector<std::string> lines);

		bool hasKey(const std::string& key) const;
		bool hasParam(const std::string& key) const;

		std::string getValue(const std::string& key) const;
		std::string getParam(const std::string& key) const;

		std::vector<std::string> getKeys() const;

		std::string toString() const;

	private:
		void internalParse(std::vector<std::string> lines);

		mutable std::map<std::string, std::pair<std::string, std::string>> mKeys;
};

}
}

#endif
